Document: nted-es
Title: NtEd documentation, Spanish translation
Author: Jörg Anders, Massimiliano G. G.
Abstract: Full documentation for the musical score editor NtEd.
Section: Sound

Format: HTML
Index: /usr/share/doc/nted/es/index.html
Files: /usr/share/doc/nted/es/*.html
